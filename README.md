Legend of Zelda – Let’s do the Puzzle
=============
I creatively manage the game while I’m browsing how to fix the debug Java script. The legend of Zelda and the Minesweeper is here! 

Using the simpler script for connecting, I used the array and [silicone grilling gloves](http://www.amazon.com/Luxe-Grill-Silicone-Resistant-Grilling/dp/B0113H87LW/); I create a set which also moves the game accordingly. The recursion of the Minesweeper is one of the examples I include, hopefully it will work though the full demonstration is still in progress. Map and list of tools in the Legend of Zelda are here.

<br/>
<table>
    <tr>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/zelda8.png" width="400px"/>
        </td>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/zelda5.png" width="400px"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/zelda7.png" width="400px"/>
        </td>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/zelda11.png" width="400px"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/zelda9.png" width="400px"/>
        </td>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/zelda10.png" width="400px"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/zelda12.png" width="400px"/>
        </td>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/zelda13.png" width="400px"/>
        </td>
    </tr>
</table>

MineSweeper
============
<table>
    <tr>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/minesweeper_winning.jpg" width="400px"/>
        </td>
        <td>
            <img src="https://raw.github.com/kennycason/java_games/master/doc/screenshots/minesweeper1.png" width="400px"/>
        </td>
    </tr>
</table>


**Required Libraries**<br/>
Java 1.7<br/>
jsoup-1.7.1.jar<br/>
log4j-1.2.17.jar<br/>
Maps written using Tiled<br/>